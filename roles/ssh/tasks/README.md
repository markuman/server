# ssh

1. allow login with signed public key (no pub key need to be transfer to the server)
2. login with password and oath-hotp

# yubi key setup

0. generate secret
  * `dd if=/dev/random bs=1k count=1 | sha1sum`
1. flash yubi key slot 2 with generated secret
  * `ykpersonalize -2 -o oath-hotp -o oath-hotp8 -o append-cr -a <SECRET>`

# sign ssh key

Generate a new ssh key **with password**.  
Sign your public key with the private key. The cert pub key will be valid for one week.  

`ssh-keygen -s ~/.ssh/<your new private key> -I <my host> -n <my user> -V +1w ~/.ssh/<your default pub key>`
