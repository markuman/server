# bootstrap my servers with basic configuration

`ansible-pull -U https://git.osuv.de/m/server -i inventories/local.ini localhost.yml --extra-vars='{"USER": "user", "PASSWORD": "password"}'`

Supported Distributions:
  * Ubuntu 20.04
  * Manjaro/Archlinux


## Requirements
### Ubuntu 20.04
```shell
#!/bin/bash
apt update
apt install git ansible -y
ln -s /usr/bin/python3 /usr/bin/python
ansible-pull -U https://git.osuv.de/m/server -i inventories/local.ini localhost.yml \
  --extra-vars='{"USER": "user", "PASSWORD": "password"}'
```

### Archlinux 

```shell
#!/bin/bash
sudo pacman -Sy git ansible -y
sudo ansible-pull -U https://git.osuv.de/m/server -i inventories/local.ini localhost.yml
```
