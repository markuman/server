.PHONY: help test

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

clean: ## delete all *.retry files
	find . -name '*.retry' -delete

run: ## start test container
	docker run \
		--name ubuntu \
		--rm -d  \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
		--cap-add SYS_ADMIN  \
		--privileged test /sbin/init

test: ## run playbook against local docker container
	ansible-playbook -i inventories/test.ini localhost.yml